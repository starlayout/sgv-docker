#!/bin/bash

if [ -z ${1+x} ]; then
	sudo docker exec --user root -it tomcat bash 
else
	sudo docker exec --user root tomcat bash -c "$1"
fi

